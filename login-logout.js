

const express = require('express');
const bodyParser = require('body-parser');
var fs = require('fs');
var userFile = require('./user.json');

const app = express();

const URL_BASE = '/apitechu/v2/';

const PORT = process.env.PORT || 3001;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Escucha de puerto
app.listen(PORT, function () {
    console.log('APITechU escuchando desde Peru en puerto 3001');
});

app.post(URL_BASE + 'login', function(req, res){

  let userFound = false;

  for (let user of userFile) {
      if(user.email == req.body.email && req.body.password == user.password){
          userFound = true;
          user.logged = true;
          writeUserDataToFile(userFile);
          
      }
      if(userFound){
        console.log("Login OK");
        res.send({mensaje : "Login correcto", "idUsuario" : "" + user.userID});
        break;
      }
  }  
      
  if(!userFound){
      console.log("Login failed");
      res.send({mensaje : "Login incorrecto"});
  }

});

// LOGOUT CON POST
app.post(URL_BASE + 'logout', function(req, res) {

  let userFound = false;

  for (let user of userFile) {
      if(user.userID == req.body.userID && user.logged) {
          userFound = true;
          delete user.logged;
          writeUserDataToFile(userFile);
      }
      if(userFound) {
        console.log("logout OK");
        res.send({mensaje : "Logout correcto", "idUsuario" : "" + user.userID});
        break;
      }
  }  
      
  if(!userFound) {
      console.log("Logout failed");
      res.send({mensaje : "Logout incorrecto"});
  }

  

});

// LOGOUT CON GET
app.get(URL_BASE + 'logout/:userID', function(req, res) {

    let userFound = false;
  
    console.log(req.params.userID);
    for (let user of userFile) {
        if(user.userID == req.params.userID && user.logged) {
            userFound = true;
            delete user.logged;
            writeUserDataToFile(userFile);
        }
        if(userFound) {
          console.log("logout OK");
          res.send({mensaje : "Logout correcto", "idUsuario" : "" + user.userID});
          break;
        }
    }  
        
    if(!userFound) {
        console.log("Logout failed");
        res.send({mensaje : "Logout incorrecto"});
    }
  
  });


//peticion GET de todos los 'users' (Collections)

app.get(URL_BASE + 'users', function(req, res){

    let isLogged = req.query.logged;
    let size = req.query.size;
    let valorInferior = req.query.inf;
    let valorSuperior = req.query.sup;


    let contUsers = 0;
    let usersLogged = [];
    let usersFound = [];

    console.log(req.query.logged);
    console.log(req.query.size);
    console.log(req.query.inf);
    console.log(req.query.sup);

    if(valorInferior !== undefined && valorSuperior !== undefined){

      if (valorInferior > valorSuperior) {
        res.send({"Mensaje" : "Valor inferior es mayor a valor superior"})
      } else {
        for( let user of userFile){
          if(user.userID >= valorInferior && user.userID <= valorSuperior){
            usersFound.push(user);
          }
          if(user.userID > valorSuperior) break;
        }
        res.send(usersFound);
      }

    } else {

      if(isLogged !== undefined && size !== undefined){

        for(let user of userFile){
            if(user.logged){
                usersLogged.push(user);
                contUsers++;
            }
    
            if(size == contUsers){
                break;
            }
    
        }

        res.send(usersLogged);
    } else {
        console.log('Obtener usuarios!!');
        res.send(userFile);

    }

    }





    

});



function writeUserDataToFile(data) {

    var jsonUserData = JSON.stringify(data);
 
    fs.writeFile("./user.json", jsonUserData, "utf8",
     function(err) { //funci�n manejadora para gestionar errores de escritura
       if(err) {
         console.log(err);
       } else {
         console.log("Datos escritos en 'user.json'.");
         console.log(data);
       }
     })
  }




function reqEmpty(req) {

    return Object.keys(req.body).length !== 0 ? true : false;

}
