var express = require('express');
var bodyParser = require('body-parser');
var app = express();

require('dotenv').config();
const apiKey = process.env.MLAB_API_KEY;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var requestJSON = require('request-json');
const URL_BASE = '/apitechu/v3/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu19db/collections/';
const PORT = process.env.PORT || 3003;

const logout_controller = require('./controllers/logout_controller');
const login_controller = require('./controllers/login_controller');

//POST LOGIN
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3003/apitechu/v3/login -d '{"email" : "rportocarrero@bbva.com", "password" : "pepito"}' | jq
app.post(URL_BASE + 'login', login_controller.postLogin);

//POST LOGOUT
// curl -s -H 'Content-Type: application/json' -X POST http://localhost:3003/apitechu/v3/logout -d '{"email" : "rportocarrero@bbva.com", "password" : "pepito"}' | jq
app.post(URL_BASE + 'logout', logout_controller.postLogout);

app.listen(PORT, function () {
  console.log('API escuchando en el puerto 3003...');
});

