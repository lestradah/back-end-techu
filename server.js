
var express = require('express');

const bodyParser = require('body-parser');

var userFile = require('./user.json');

var totalUsers = 0;

var fs = require('fs');

var app = express();

const URL_BASE = '/apitechu/v1/';

const PORT = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Escucha de puerto
app.listen(PORT, function () {
    console.log('APITechU escuchando desde Peru en puerto 3000');
});


//peticion GET de todos los 'users' (Collections)
app.get(URL_BASE + 'users', function (req, res) {
    console.log('Obtener usuarios!!');
    res.send(userFile);

});

//peticion GET para un 'user' (Instance)
app.get(URL_BASE + 'users/:id', function (req, res) {

    let indice = req.params.id;

    let userFound = userFile[indice - 1];

    console.log('GET ' + URL_BASE + req.params.id);

    let respuesta = (userFound !== undefined) ? userFound : { "mensaje": "Objeto no encontrado" };

    //res.status();
    res.send(respuesta);

});


//peticion GET para un 'user' (Instance)
app.get(URL_BASE + 'usersq', function (req, res) {

    let indice = req.params.id;

    let userFound = userFile[indice - 1];

    console.log('GET ' + URL_BASE + ' con query string');
    console.log(req.query);
});

app.post(URL_BASE + 'users', function (req, res) {

    console.log('Este es el request' + JSON.stringify(req.body));

    totalUsers = userFile.length;

    if (reqEmpty(req)) {
        let newUser = {
            "userID": totalUsers + 1,
            "first_name": req.body.first_name,
            "last_name": req.body.last_name,
            "email": req.body.email,
            "password": req.body.password
        };

        userFile.push(newUser);
        res.status(200);
        res.send({ "mensaje": "Usuario creado " + JSON.stringify(userFile[newUser.userID - 1]) });

    } else {

        res.send({ "mensaje": "No se tiene body" });

    }

});


//Implementacion de PUT
app.put(URL_BASE + 'users', function (req, res) {


    if (reqEmpty(req)) {

        let userFound = userFile[req.body.userID - 1];
        userFound.first_name = req.body.first_name;
        userFound.last_name = req.body.last_name;
        userFound.email = req.body.email;
        userFound.password = req.body.password;

        console.log('Usuario a actualizar ' + JSON.stringify(userFound));

        userFile.splice(req.body.userID - 1, 1, userFound);

        res.status(201);
        res.send({ "mensaje": "usuario reemplazado " + JSON.stringify(userFile[req.body.userID - 1]) });


    } else {

        res.send({ "mensaje": "No se tiene body" });

    }



});


app.delete(URL_BASE + 'users', function (req, res) {

    if(reqEmpty(req)){
        console.log('Usuario a eliminar ' + JSON.stringify(req.body.userID));

        userFile.splice(req.body.userID - 1, 1);
    
        res.status(200);
        res.send({ "mensaje": "usuario eliminado " + JSON.stringify(userFile) });

    } else {
        res.send({ "mensaje": "No se tiene body" });
        
    }

});


app.delete(URL_BASE + 'users/:id', function(req, res){

    if(req.params.id <= userFile.length){
        console.log('Usuario a eliminar ' + JSON.stringify(req.params.id));

        userFile.splice(req.params.id - 1, 1);
    
        res.status(200);
        res.send({ "mensaje": "usuario eliminado " + JSON.stringify(userFile) });

    } else {
        res.send({ "mensaje": "No se tiene usuario identificado" });
    }

});


app.post(URL_BASE + 'login', function(req, res){

    let userFound = false;

    for (let user of userFile) {
        if(user.email == req.body.email && req.body.password == user.password){
            userFound = true;
            user.logged = true;
            writeUserDataToFile(userFile);
            break;
        }
    }


    if(userFound){
        console.log("Login OK");
        res.send({mensaje : "Login exitoso", "user" : req.body.email});
        
    } else {
        console.log("Login failed");
        res.send({mensaje : "usuario o password incorrecto"});
    }

});

app.delete(URL_BASE + 'logout', function(req, res){

    let userFound = false;

    for (let user of userFile) {
        if(user.email == req.body.email && user.logged){
            userFound = true;
            delete user.logged;
            writeUserDataToFile(userFile);
            break;
        }
    }


    if(userFound){
        console.log("Logout OK");
        res.send({mensaje : "Logout exitoso", "user" : req.body.email});
        
    } else {
        console.log("Logout failed");
        res.send({mensaje : "usuario no logeado"});
    }

});

function writeUserDataToFile(data) {

    var jsonUserData = JSON.stringify(data);
 
    fs.writeFile("./user.json", jsonUserData, "utf8",
     function(err) { //función manejadora para gestionar errores de escritura
       if(err) {
         console.log(err);
       } else {
         console.log("Datos escritos en 'user.json'.");
         console.log(data);
       }
     })
  }




function reqEmpty(req) {

    return Object.keys(req.body).length !== 0 ? true : false;

}
