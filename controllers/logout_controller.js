module.exports.postLogout = postLogout;
var requestJSON = require('request-json');
const URL_BASE = '/apitechu/v3/';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu19db/collections/';

require('dotenv').config();
const apiKey = process.env.API_KEY_MLAB;

  function postLogout(req, res){
  let email = req.body.email;
  //let pass = req.body.password;
  let queryString = 'q={"email":"' + email + '"}&';
  let limFilter = 'l=1&';
  let httpClient = requestJSON.createClient(URL_MLAB);
  httpClient.get('user?'+ queryString + limFilter + 'apiKey=' + apiKey,
    function(err, respuestaMLab, body) {
     console.log(URL_MLAB, 'user?' + queryString + limFilter + 'apiKey=' + apiKey); 
     console.log(err, body);
      if(!err) {
        if (body.length == 1) { // Existe un usuario que cumple 'queryString'
          let logout = '{"$unset":{"logged":true}}';
          httpClient.put('user?q={"id": ' + body[0].id + '}&' + 'apiKey=' + apiKey, JSON.parse(logout),
          //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
            function(errPut, resPut, bodyPut) {
              res.send({'msg':'LogOut correcto', 'user':body[0].email, 'userid':body[0].id});
              // If bodyPut.n == 1, put de mLab correcto
            });
        }
        else {
          res.status(404).send({"msg":"Usuario no válido."});
        }
      } else {
        res.status(500).send({"msg": "Error en petición a mLab."});
      }
  });
};
