

const express = require('express');
const bodyParser = require('body-parser');
var fs = require('fs');
var userFile = require('./user.json');
var rqMlab = require('request-json');

require('dotenv').config();

const app = express();

const URL_BASE = '/apitechu/v3/';

const URL_MLAB      = 'https://api.mlab.com/api/1/databases/apitechuelb5ed/collections/';
const URL_MLAB_LUIS      = 'https://api.mlab.com/api/1/databases/techu18db/collections/';

const API_KEY_MLAB = process.env.API_KEY_MLAB;

const API_USER = 'users';
var filtro = 'f={"_id":0}&s={"usuarioID":1}';
var queryReq = '';
var clientMlab = rqMlab.createClient(URL_MLAB_LUIS);


const PORT = process.env.PORT || 3001;

app.use(bodyParser.json());

//Escucha de puerto
app.listen(PORT, function () {
    console.log('APITechU escuchando desde Peru en puerto 3001');
});


const user_controller = require("./controllers/user_controller");


app.get(URL_BASE + API_USER, user_controller.getUsers);



//parametro ID
app.get(URL_BASE + API_USER + '/:id', function(req, res){

    
    if(req.params.id){
        queryReq = '&q={"usuarioID":' + req.params.id + '}';
    }
    
    console.log(req.params.id);

    var reqStr = 'user/' + '?' + filtro + queryReq + '&apiKey=' + API_KEY_MLAB;

    console.log(URL_MLAB + reqStr);

    clientMlab.get(reqStr, function(err, resp, body){

        if(err){
            console.log(err);
            res.send({mensaje : "Error al consultar a MLab"});
            res.status(resp.statusCode)

        } else {
            res.status(resp.statusCode);
            res.send(body);

        }
    });
});


//get accounts
app.get(URL_BASE + API_USER + '/:id/accounts', function(req, res){

    
    if(req.params.id){
        queryReq = '&q={"userID":' + req.params.id + '}';
    }
    
    console.log(req.params.id);

    var reqStr = 'account/' + '?' + filtro + queryReq + '&apiKey=' + API_KEY_MLAB;

    console.log(URL_MLAB + reqStr);

    clientMlab.get(reqStr, function(err, resp, body){

        if(err){
            console.log(err);
            res.send({mensaje : "Error al consultar a MLab"});
            res.status(resp.statusCode)

        } else {
            res.status(resp.statusCode);
            res.send(body);

        }
    });
});

//POST MLAB
app.post(URL_BASE + API_USER, function(req, res){

    var reqStr = 'user/' + '?' + filtro  + '&apiKey=' + API_KEY_MLAB;

    clientMlab.get(reqStr, function(err, resp, body){

        indexNewUser = body.length + 1;

        var newUser = { usuarioID :   indexNewUser,
                        name       : req.body.name,
                        lastName   : req.body.lastName,
                        email      : req.body.email,
                        password   : req.body.password
                        };

        var newReqStr = 'user/' + '?' + 'apiKey=' + API_KEY_MLAB;

        clientMlab.post(newReqStr, newUser, function(err, resp, body){

            if(err){
                res.send({mensaje : "error add client"});
                res.status(resp.statusCode);
            } else {
                res.send(body);
                res.status(resp.statusCode);
            }
        });
    });
});


//PUT MLAB
app.put(URL_BASE + API_USER + '/:id', function(req, res){

    var newReqStr = 'user/' + '?q={"usuarioID":'+  req.params.id  +  '}&apiKey=' + API_KEY_MLAB;

    var userUpdate = '{"$set" :' + JSON.stringify(req.body) + '}';

    clientMlab.put(newReqStr, JSON.parse(userUpdate), function(err, response, body){
        
        res.status(response.statusCode);

        console.log(body);
        !err ? res.send(body) : res.send({mensaje : "error en PUT"});

    });

});


//DELETE MLAB
app.delete(URL_BASE + API_USER + '/:id', function(req, res){

    var newReqStr = 'user/' + '?q={"usuarioID":'+  req.params.id  +  '}&apiKey=' + API_KEY_MLAB;

    clientMlab.get(newReqStr, function(err, response, body){

        var userFound = body[0];

        console.log(userFound);
        console.log(userFound._id.$oid);
        clientMlab.delete('user/' + userFound._id.$oid + '?apiKey=' + API_KEY_MLAB, function(err, response, body){

            res.send(body);

        });

    });

});


//LOGIN MLAB

app.post(URL_BASE + API_USER, function(req, resp){

    req.body.email;

    reqStr = 'user/?q';

    clientMlab.get();



    var newReqStr = 'user/?q={"usuarioID":'+  req.params.id  +  '}&apiKey=' + API_KEY_MLAB;

    var userUpdate = '{"$set" :' + JSON.stringify(req.body) + '}';

    clientMlab.put()

})













